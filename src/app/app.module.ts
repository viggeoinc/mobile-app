import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {GameDetailsPage} from "../pages/game-details/game-details";
import {ProfilePage} from "../pages/profile/profile";
import {GameAddPage} from "../pages/game-add/game-add";

let pages: any = [
    MyApp,
    HomePage,
    TabsPage,
    GameDetailsPage,
    GameAddPage,
    ProfilePage
];

@NgModule({
    declarations: pages,
    entryComponents: pages,
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
