import { Component } from '@angular/core';
import {ViewController} from 'ionic-angular';

@Component({
    selector: 'page-game-add',
    templateUrl: 'game-add.html'
})
export class GameAddPage {

    constructor(public viewCtrl: ViewController) {

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
