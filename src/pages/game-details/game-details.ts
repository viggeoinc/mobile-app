import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-game-details',
    templateUrl: 'game-details.html'
})
export class GameDetailsPage {

    constructor(public navCtrl: NavController) {

    }

}
